# Automatically Decrapify Windows 10

## Purpose

These scripts are not made to clean *everything* in **Windows 10**.

Rather, the goal is to make Windows enterprise-ready, while minimizing the risks of breaking things.

## What It Does

1. Clear all the start menu tiles *(for future users only)*.
1. Disable app suggestions.
Windows 10 will no longer offer new apps from the start menu.
1. Disable silent app install.
Windows 10 will no longer install new apps without asking the users.
1. Remove all default apps deemed unnecessary for the enterprise.
See [unwanted_default_apps.txt](content/unwanted_default_apps.txt) for the complete list.
1. Remove all extra apps deemed unnecessary for the enterprise.
See [unwanted_extra_apps.txt](content/unwanted_extra_apps.txt) for the complete list.

*The choice of apps to keep is subjective, and may not suit everyone.*

## How to Run

1. Open **Command Prompt** or **PowerShell** as admin.
1. Execute `run.bat`
1. That's it!

## Compatibility

Updated for **Windows 10 Pro, build 1903**.

## License

Please see the [LICENSE](LICENSE) file.

## Reference Material

- https://docs.microsoft.com/en-us/windows/application-management/apps-in-windows-10
- https://blog.danic.net/provisioned-app-packages-in-windows-10-enterprise-windows-10-pro/
- https://vacuumbreather.com/index.php/blog/item/87-windows-10-1903-built-in-apps-what-to-keep

## Logo Attribution

<div>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
